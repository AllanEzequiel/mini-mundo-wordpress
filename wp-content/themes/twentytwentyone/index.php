<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header(); ?>
	

	
	
	<main class="container_newspaper">
		<section class="newspaper_mini_world">
			<?php 
			  	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'category_name' => 'blog',
					'post_status' => 'publish',
					'paged' => $paged 
				); 
			?>
			<?php query_posts($args); ?>
			<h1 id="knowmore" >Blog</h1>
			<p>Confira as últimas notícias, os eventos e as novidades do Mini Mundo.</p>

			<?php 
				wp_nav_menu(
					array(
						'theme_location'  => 'menu_tags',
						'items_wrap'      => '<ul class="tag_container %2$s">%3$s</ul>',
						'fallback_cb'     => false,
					)
				);
			?>
				<?php
					if ( have_posts() ) {
						?> <div class="news_gallery"> <?php
							// Load posts loop.
							while ( have_posts() ) {
								the_post();

								get_template_part( 'template-parts/content/content', get_theme_mod( 'display_excerpt_or_full_post', 'excerpt' ) );
							};
						?></div><?php
						learnplus_blog_pagination();

					} else {

						// If no content, include the "No posts found" template.
						get_template_part( 'template-parts/content/content-none' );

					};
				?>			
			
		</section>
	</main>
<?php


get_footer();
