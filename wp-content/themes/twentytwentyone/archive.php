<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();


		?>
		<?php
		$description = get_the_archive_description();
		?>

		<?php if ( have_posts() ) : ?>
		<main>
			<section class="newspaper_mini_world">
				
				<?php $post_tags = get_the_tags(); echo  '<h1 id="knowmore">' . esc_html( $post_tags[0]->name ) . '</h1>';?>
				<p><?php if ( $description ) : ?></p>
					<p><?php echo wp_kses_post( wpautop( $description ) ); ?></p>
					
				<?php endif; ?>
				<?php 
						wp_nav_menu(
							array(
								'theme_location'  => 'menu_tags',
								'items_wrap'      => '<ul class="tag_container %2$s">%3$s</ul>',
								'fallback_cb'     => false,
							)
						);
					?>

				
				<div class="news_gallery">
					<?php while ( have_posts() ) : ?>
						<?php the_post(); ?>
						<?php get_template_part( 'template-parts/content/content', get_theme_mod( 'display_excerpt_or_full_post', 'excerpt' ) ); ?>
						
					<?php endwhile; ?>
				</div>
				<?php learnplus_blog_pagination(); ?>

				
				
			</section>			
		</main>

	

<?php else : ?>
	<?php get_template_part( 'template-parts/content/content-none' ); ?>
<?php endif; ?>

<?php get_footer(); ?>
