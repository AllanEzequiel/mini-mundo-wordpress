<?php
/**
 * The template for displaying all single posts
 * Template Name: App
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();?>
    <main>
        <section class="container_download_App">
            <h1>
                Baixe o App e sinta-se um Mini-Habitante
            </h1>

            <div class="download_App">
                <div class="information">
                   
                    <p>
                        Utilize o nosso app oficial para ter uma experiência única durante a sua visita. Com o aplicativo você consegue ter a visão em 360° e navegar pela mini-cidade como um mini-habitante. Além disso, você tem acesso ao mapa e informações do parque.
                    </p>
                    <a href='https://itunes.apple.com/br/app/mini-mundo/id1283255770?mt=8' target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/appstore_icon.png" alt='Disponível na Apple Store'/>
                    </a>
                    <a class="googleplay" href='https://play.google.com/store/apps/details?id=winckler.srv.br.minimundo' target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/googleplay_icon.png" alt='Disponível no Google Play'/>
                    </a>
                </div>
                <div class="image_app">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/baixe-o-app.png" alt="">
                </div>
            </div>
        </section>
    </main>

<?php get_footer();
