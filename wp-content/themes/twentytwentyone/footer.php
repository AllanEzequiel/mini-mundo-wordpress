<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
	<footer>
		<!-- ASKSUITE CODE START --> <script id="script-infochat" src='https://cdn.asksuite.com/infochat.js?dataConfig=https://control.asksuite.com/api/companies/mini-mundo'></script> <!-- ASKSUITE CODE END -->

		<div class="emailbox">
			<h2>Receba as novidades do Mini Mundo</h2>
			<p class="desktop_text">Deixe seu e-mail e fique por dentro de notícias, eventos e novidades do parque.</p>
			<p class="mobile_text">Para ficar por dentro de notícias, eventos e novidades do parque, assine a nossa newsletter.</p>
			<div>
				<?php
					echo do_shortcode( '[contact-form-7 id="519" title="E-mail Form"]' );
				?>
			</div>
		</div>
		<div class="informationFooter">
			<div class="site_map">
				<h3>Mapa do site</h3>
				<?php 
					wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
							'fallback_cb'     => false,
						)
					);
				?>
			</div>

			<div class="contact">
				<h3>Contato</h3>
				<ul>
					<li>
						parque@minimundo.com.br
					</li>
					<li>
						minimundo@minimundo.com.br
					</li>
					<li>
						(54) 3286.4055
					</li>
				</ul>

				<ul class="social_networks">
					<li class="facebook">
						<a href="https://www.facebook.com/MiniMundoGramado" target="_blank"></a>
					</li>
					<li class="youtube">
						<a href="https://www.youtube.com/channel/UC9CwtXghVifmhJKFkuz6CHA" target="_blank"></a>
					</li>
					<li class="instagram">
						<a href="https://www.instagram.com/minimundogramado/" target="_blank"></a>
					</li>
				</ul>
				<h3>Horário de funcionamento</h3>
				<p>Aberto todos os dias das 9h às 17h</p>
			</div>

			<div class="localization">
				<h3>Localização</h3>
				<p>
					Rua Horácio Cardoso, 291
					<br>
					CEP 95670-000
					<br>
					Gramado - RS - Brasil
					<br>
				</p>
				<iframe 
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13906.054499232629!2d-50.8754059!3d-29.3845272!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x11b129a1a6182575!2sMini%20Mundo!5e0!3m2!1spt-BR!2sbr!4v1652793947920!5m2!1spt-BR!2sbr"
                    width="600"
                    height="450" 
                    style="border:0;" 
                    allowfullscreen="" 
                    loading="lazy" 
                    referrerpolicy="no-referrer-when-downgrade"
                    >
                </iframe>
			</div>
		</div>
		<h2>Certificações e iniciativas</h2>
		<div class="certificates_images">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="copyright">
			Mini Mundo © 2022. Todos os direitos reservados
		</div>
	</footer>
	<script src="<?php echo get_template_directory_uri(); ?>/js/inline.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.inputmask.js"></script>
<?php wp_footer(); ?>


</body>
</html>
