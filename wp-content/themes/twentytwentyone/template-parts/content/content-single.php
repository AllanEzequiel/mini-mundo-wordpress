<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
		
		
		


		

		

		

	<main>
		<section class="news">

			<?php $tags = get_the_tags();
				if ( ! empty( $tags ) ) {
					echo '<a class="category_link" href="' . esc_url( get_tag_link( $tags[0]->term_id ) ) . '">' . esc_html( $tags[0]->name ) . '</a>';
				}
			?>

			<?php the_title( '<h1 id="knowmore" class="entry-title">', '</h1>' ); ?>
			
	


			<div class="news_imageCard">
				
				<p>
					Por <span><?php echo get_the_author(); ?></span>, publicado em <?php echo get_the_date('d/m/y'); ?> às <?php echo date('h:i'); ?>.
				</p>
				<div class=containerImage>
					<?php the_post_thumbnail('full'); ?>
				</div>
			</div>

			<div class="description">
				<?php the_content(); ?>
			
				<div class="share">
					Compartilhe
					<ul>
						<li class="facebook">
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink();?>" target="_blank"></a>
						</li>
						<li class="whatsapp">
							<a href="whatsapp://send?text=<?php the_title(); ?>%0a%0aLink:<?php echo get_permalink();?>" ></a>
						</li>
					</ul>
				</div>
			</div>
		</section>
		<?php 	
			$isjornal = do_shortcode( '[pods field="_field.este_post_e_um_jornal"]' );
			if ($isjornal == "Este post é um jornal") { ?>
				
				<section class="relatedEdits">
					<h2>Outras edições</h2>
					<div class="carousel_NewsEdits">
						<?php 
							$pod_jornal = pods( 'jornal' );
							$params = array(     
								'limit' => -1,   
								'orderby' => 'post_date DESC',
							); 
							$pod_jornal = pods( 'jornal', $params ); 
							while ( $pod_jornal->fetch() ) { ?>
							<div class="editionCard newsId_<?php echo $pod_jornal->display( 'id' );?>">
								<div class=information>
									<h3><?php echo $pod_jornal->display( 'title' );?></h3>
									<?php echo $pod_jornal->display( 'excerpt' );?>
									<a href="<?php echo $pod_jornal->display( 'permalink' );?>">LER EDIÇÃO</a>
								</div>
								<div class=news>
									<img src="<?php echo $pod_jornal->display( 'imagem' );?>" alt="<?php echo $pod_jornal->display( 'title' );?>">
								</div>
							</div>
						<?php } ?>
					</div>
				</section>

		<?php } else { ?>

			<section class="related_news">
				<h2>Notícias relacionadas</h2>
				<div class="container_related_news">

                    <?php
                    $tags = wp_get_post_terms( get_queried_object_id(), 'post_tag', ['fields' => 'ids'] );
                    $args = [
                        'post__not_in'        => array( get_queried_object_id() ),
                        'posts_per_page'      => 3,
                        'ignore_sticky_posts' => 1,
                        'orderby'             => 'DESC',
                        'tax_query' => [
                            [
                                'taxonomy' => 'post_tag',
                                'terms'    => $tags
                            ]
                        ]
                    ];
                    $my_query = new wp_query( $args );
                    if( $my_query->have_posts() ) {
                        while( $my_query->have_posts() ) {
                            $my_query->the_post(); ?>
                                <a href="<?php echo get_permalink();?>">
                                    <div class="card_image">
                                        <?php the_post_thumbnail(); ?>
                                    </div> 
                                    <div class="newsInformation">
                                    <span><?php $post_tags = get_the_tags();
										if ( $post_tags ) {
											echo $post_tags[0]->name; 
										}
                                    ?></span>
                                        <p><?php the_title();?></p>
                                        <?php echo get_the_date('d/m/y'); ?>
                                    </div>
                                </a>
                        <?php }
                        wp_reset_postdata();
                    }
                    ?> 

				</div>	
				<a href="<?php echo home_url();?>/blog" class="button_more">
					<p>ver todas as notícias</p>
				</a>
			</section>
		<?php } ?>
		
		<script>
                $('.newsId_<?php echo get_the_ID() ?>').addClass('hideCardNews')

		</script>
			
	</main>
