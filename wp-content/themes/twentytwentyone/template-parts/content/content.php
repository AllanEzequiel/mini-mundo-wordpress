<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
UMA COISA 5

	<?php if ( is_singular() ) : ?>
		<?php the_title( '<h1 class="entry-title default-max-width">', '</h1>' ); ?>
	<?php else : ?>
		<?php the_title( sprintf( '<h2 class="entry-title default-max-width"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	<?php endif; ?>

	<?php twenty_twenty_one_post_thumbnail(); ?>


		<?php
		the_content(
			twenty_twenty_one_continue_reading_text()
		);

		?>
