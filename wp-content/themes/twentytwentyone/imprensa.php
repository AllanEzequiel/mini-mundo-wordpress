<?php
/**
 * The template for displaying all single posts
 * Template Name: Imprensa
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();?>

    <main>
        <section class="dowloadGallery">
            <h1>Baixe as nossas imagens</h1>
            <p class="text_center">Abaixo você tem acesso à galeria de imagens do Mini Mundo.</p>

            <div class="gridGallery">
                <?php 
                    $pod_galeria_de_download = pods( 'galeria_de_download' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date DESC',
                    ); 
                    $pod_galeria_de_download = pods( 'galeria_de_download', $params ); 
                    while ( $pod_galeria_de_download->fetch() ) { ?>
                        <div class="cardDownload">
                            <img src="<?php echo $pod_galeria_de_download->display( 'imagem' );?>" alt="<?php echo $pod_galeria_de_download->display( 'title' );?>">
                            <a href="https://drive.google.com/uc?export=download&id=<?php echo $pod_galeria_de_download->display( 'id_do_arquivo' );?>" dowload>Baixar imagem</a>
                        </div>
                <?php } ?>
            </div>

        </section>
    </main>    

<?php get_footer();
