<?php
/**
 * The template for displaying all single posts
 *Template Name: Planeje sua visita
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();?>
    <main>
        <section class="for_you_apresentation miniMundo_apresentation showSection">
            <div class="background-carousel">
                <?php 
                    $banner_planeje = pods( 'banner_planeje_sua_v' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date DESC',
                    ); 
                    $banner_planeje = pods( 'banner_planeje_sua_v', $params ); 
                    while ( $banner_planeje->fetch() ) {    ?>
                        <div class="background_image">
                            <img class="desktop_image" src="<?php echo $banner_planeje->display( 'banner_desktop'); ?>" alt="<?php echo $banner_planeje->display( 'title' ); ?>">
                            <img class="mobile_image" src="<?php echo $banner_planeje->display( 'banner_mobile'); ?>" alt="<?php echo $banner_planeje->display( 'title' ); ?>">
                        </div>
                <?php } ?>       
            </div>

            <div class="miniMundo_information">
                <h1>Economize e parcele em até 6x, garantindo seus ingressos antecipados.</h1>
                <p>Adquirindo seus ingressos com até 24h de antecedência, você garante a diversão por um preço ainda melhor do que comprando na hora!</p>
            </div>
            <a href="https://loja.minimundo.com.br/" target="_blank">Ver Ofertas</a>
            <span id="knowmore"></span>
        </section>
        <section class="mobile_information">
            <div class="miniMundo_information">
                <h2 class="title">Economize e parcele em até 6x, garantindo seus ingressos antecipados.</h2>
                <p>Adquirindo seus ingressos com até 24h de antecedência, você garante a diversão por um preço ainda melhor do que comprando na hora!</p>
            </div>
        </section>

        <section class="prepare_visit">
            <h2>Prepare-se para sua visita</h2>
            <p class="text_center">Separamos algumas dicas para tornar sua visita ainda mais especial.</p>

            <div class="advise">
                <?php 
                $pods_prepare_sua_visita = pods( 'prepare_sua_visita' );
                $params = array(     
                    'limit' => -1,   
                    'orderby' => 'post_date DESC',
                ); 
                $pods_prepare_sua_visita = pods( 'prepare_sua_visita', $params ); 
                while ( $pods_prepare_sua_visita->fetch() ) {    ?> 
                    <div class="card">
                        <div class="cardImage">
                            <img src="<?php echo $pods_prepare_sua_visita->display( 'imagem');?>" alt="<?php echo $pods_prepare_sua_visita->display( 'title');?>" height="100">
                        </div>
                        
                        <div class="cardInformation">
                            <h4><?php echo $pods_prepare_sua_visita->display( 'title');?></h4>
                            <?php echo $pods_prepare_sua_visita->display( 'content');?>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </section>

        <section class="convenience">
            <h2>Infraestrutura</h2>
            <p class="text_center">Nossa estrutura completa te aguarda para eternizar momentos de muita diversão.</p>

            <div class="carousel_convenience">
                
                <?php 
                    $pods_comodidade = pods( 'comodidade' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date DESC',
                    ); 
                    $pods_comodidade = pods( 'comodidade', $params ); 
                    while ( $pods_comodidade->fetch() ) {    ?> 
                        <div class="convenience_card">
                            <div class="card_image">
                                <img src="<?php echo $pods_comodidade->display( 'imagem' );?>" alt="<?php echo $pods_comodidade->display( 'title' );?>"> 
                            </div>
                            <div class="information_convenience">
                                <h3><?php echo $pods_comodidade->display( 'title' );?></h3>
                                <?php echo $pods_comodidade->display( 'content' );?>
                            </div>
                        </div>
                <?php } ?>
            </div>
        </section>

        <!-- <section class="way_map">
            <span></span>
            <h2>Já sabe como chegar?</h2>
            <a href="<?php echo home_url();?>/contato">ver mapa</a>
        </section> -->

        <!-- <section class="container_download_App">
            <div class="download_App">
                <div class="information">
                    <h2>
                        Baixe o App e sinta-se um Mini-Habitante
                    </h2>
                    <p>
                        Utilize o nosso app oficial para ter uma experiência única durante a sua visita. Com o aplicativo você consegue ter a visão em 360° e navegar pela mini-cidade como um mini-habitante. Além disso, você tem acesso ao mapa e informações do parque.
                    </p>
                    <a href='https://itunes.apple.com/br/app/mini-mundo/id1283255770?mt=8' target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/appstore_icon.png" alt='Disponível na Apple Store'/>
                    </a>
                    <a class="googleplay" href='https://play.google.com/store/apps/details?id=winckler.srv.br.minimundo' target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/googleplay_icon.png" alt='Disponível no Google Play'/>
                    </a>
                </div>
                <div class="image_app">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/baixe-o-app.png" alt="">
                </div>
            </div>
        </section> -->

        <section class="tabs">
            <div class="tab_buttons">
                <span class="book_button active">Conheça nossos livros</span>
                <span class="download_button">Leituras educativas</span>
                <span class="underline"></span>

                <span class="mobile book_button active">Conhecer livros</span>
                <span class="mobile download_button">Ler livros</span>
                <span class="mobile underline"></span>
            </div>
            
            <div class="carouselContainer">
                <div class="carousel_cardBook active">
                    
                    <?php 
                        $pods_comprar_livro = pods( 'comprar_livro' );
                        $params = array(     
                            'limit' => -1,   
                            'orderby' => 'post_date DESC',
                        ); 
                        $pods_comprar_livro = pods( 'comprar_livro', $params ); 
                        while ( $pods_comprar_livro->fetch() ) {    ?>      
                            <div class="card_book">
                                <div class="book_image">
                                    <img src="<?php echo $pods_comprar_livro->display( 'imagem' );?>" alt="<?php echo $pods_comprar_livro->display( 'title' );?>">
                                </div>
                                
                                <div class="book_infomation">
                                    <h2><?php echo $pods_comprar_livro->display( 'title' );?></h2>
                                    <?php echo $pods_comprar_livro->display( 'content' );?>
                                    <span><?php echo $pods_comprar_livro->display( 'autor' );?></span>                          
                                    <a href="<?php echo $pods_comprar_livro->display( 'url_comprar' );?>" target="_blank">comprar</a>
                                </div>
                            </div>
                    <?php } ?>
                </div>
                  
                <div class="carousel_downloadBook">
                    
                    <?php 
                        $pods_baixar_livro = pods( 'baixar_livro' );
                        $params = array(     
                            'limit' => -1,   
                            'orderby' => 'post_date DESC',
                        ); 
                        $pods_baixar_livro = pods( 'baixar_livro', $params ); 
                        while ( $pods_baixar_livro->fetch() ) {    ?>
                            
                            <div class="card_book">
                                <div class="book_image">
                                    <img src="<?php echo $pods_baixar_livro->display( 'imagem' );?>" alt="<?php echo $pods_baixar_livro->display( 'title' );?>"> 
                                </div>
                                <div class="book_infomation">
                                    <h2><?php echo $pods_baixar_livro->display( 'title' );?></h2>
                                    <p><?php echo $pods_baixar_livro->display( 'content' );?></p>
                                    <span><?php echo $pods_baixar_livro->display( 'autor' );?></span>
                                    <a href="<?php echo $pods_baixar_livro->display( 'download_livro' );?>" target="_blank">Ler</a>
                                </div>
                            </div>
                    <?php } ?>
                 </div>   

            </div>
        </section>
    </main>


<?php get_footer();
