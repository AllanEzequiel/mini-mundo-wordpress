// (function ($,jQuery) {
// 	"use strict";
// 	var carousels = function () {
// 		$(".background-carousel").owlCarousel({
// 			loop: true,
// 			center: false,
// 			nav: false,
// 			autoplay: true,
// 			mouseDrag: false,
// 			touchDrag: false,
// 			margin: 0,
// 			smartSpeed: 600,
// 			dots: false,
// 			responsiveClass: true,
// 			responsive: {
// 				0: {
// 				items: 1,
// 				},
// 				680: {
// 				items: 1,
// 				},
// 				1051: {
// 				items: 1,
// 				},
// 			}
// 		});
// 	};
// 	(function ($) { carousels(); })(jQuery);
// })(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_Gallery").owlCarousel({
			loop: true,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			autoplay:true,
			autoplayTimeout: 5000,
			autoplayHoverPause:false,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 0,
				autoplay: false,
				center: false,
				loop: false,
				},

				1051: {
				items: 3,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel-Newspaper").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 3,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	function owlInitialize() {
		if ($(window).width() < 1051) {
			$('.container_related_news').addClass("owl-carousel");
			$('.owl-carousel').owlCarousel({
			loop: false,
			center: false,
			nav: false,
			dots: true,
			margin: 20,
			items: 1,
		});
	} else {
		$('.owl-carousel').owlCarousel('destroy');
		$('.carousel_related_news').removeClass("owl-carousel");
	}
	
	}
	$(document).ready(function(e) {
		owlInitialize();
	});
	$(window).resize(function() {
		owlInitialize();
	});

})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_main_monuments").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 2,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_timeline_Gallery").owlCarousel({
			loop: false,
			center: true,
			nav: true,
			margin: 40,
			dots: true,
			URLhashListener:true,
			startPosition: 'URLHash',
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 1,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_mascots").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 2,
				margin: 40,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_cardBook").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 1,
				margin: 40,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_downloadBook").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 1,
				margin: 40,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_convenience").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 2,
				margin: 40,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".carousel_NewsEdits").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 40,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
				items: 1,
				margin: 20,
				},
				680: {
				items: 1,
				margin: 20,
				},
				1051: {
				items: 2,
				margin: 40,
				},
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".tag_container").owlCarousel({
			loop: false,
			center: false,
			nav: true,
			margin: 10,
			dots: false,
			autoWidth: true,
			
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {

	$('.mobile-menu').click(function(){
		$('header').toggleClass('active');
		$('.navegation').toggleClass('active');
		$('.mobile-menu').toggleClass('active');
		$('.icontickets').toggleClass('active');
	});

	$('.button_block').click(function(){
		$('.news_gallery').toggleClass('active');
		$('.button_block').toggleClass('less');
	});
	
	$('.button_block_bignews').click(function(){
		$('.bignews').toggleClass('active');
		$('.button_block_bignews').toggleClass('less');
	});

	$('.button_block').click(function(){
		$('.newspaper_content').toggleClass('active');
		$('.newspaper_content.active').toggleClass('less');
	});

	$('.button_related_news').click(function(){
		$('.container_related_news').toggleClass('active');
		$('.button_related_news').toggleClass('hide');
	});

	$('.accordion_card').click(function(){
		$(this).parent().parent().toggleClass("active");
		$(this).toggleClass('active');
		if ( $(this).is( ".active") ) {
			$('.open_close').addClass('active'); 
			
		} else {
			$('.open_close').removeClass("active");
		}
	});

	$(document).ready(function() {
		$('.accordion_card:first-child').addClass('active');
	  });

	$('#buttonForm').click(function(){
		$('span.check').parent().addClass("active");
	});


	$('.mobile-menu').click(function() {
		$('body').toggleClass('notscroll');
	});

	$('.newsInformation p').each(function () {
		len=$(this).text().length;
		if (len > 80) {
			$(this).text($(this).text().substring(0, 80) + "...");
		}
    });

	$('.book_infomation p').each(function () {
		len=$(this).text().length;
		if (len > 130) {
			$(this).text($(this).text().substring(0, 130) + "...");
		}
    });


	$('.tab_buttons span').click(function(){
		$('.tab_buttons span').removeClass("active");
		$(this).addClass("active");

		if ( $('.download_button').is( ".active") ) {
			$('.underline').addClass("active")
			$('.carousel_cardBook').removeClass("active");
			$('.carousel_downloadBook').addClass("active");
		}
		
		if ( $('.book_button').is( ".active" ) ) {
			$('.carousel_downloadBook').removeClass("active");
			$('.carousel_cardBook').addClass("active");
		}
		
	});


	$(".more_operation").each(function(){
		if (!$(this).text().trim().length) {
			$(this).parent().addClass("hide");
		}
	});

	$(".more_website").each(function(){
		if (!$(this).text().trim().length) {
			$(this).parent().addClass("hide");
		}
	});
	
	
	$(".owl-item").last().addClass("last_item")
	
	// if ($(".last_item").is(".active")) {
	// 	$(".carousel_Gallery").addClass("image_left");
	// }

	// $('.last_item').each(function(){

	// 	if ($('.last_item').last().hasClass("active")) {
	// 		$(".carousel_Gallery").addClass("image_left");
	// 	}
	// 	console.log($(".last_item"))
	// 	console.log($(".owl-item.last_item").hasClass("active"))
	// });


	// Sroll com ancora da primeira para segunda dobra

	var lastScrollTop = 0;
	$(window).scroll(function(event) {
		var scroll = $(this).scrollTop();
		if (scroll > lastScrollTop) {
			$('.showSection').addClass('hideSection');
		} 
		lastScrollTop = scroll;
	});

	setInterval(function() {
		if ($(window).scrollTop() == 0) {
			$('.miniMundo_apresentation').removeClass('hideSection');
		}; 
		
	}, 250);



	
	setInterval(function() {
		if ($(window).scrollTop() > 60) {
			$("body").addClass("scrolled");
		
			
		}; 
		if ($(window).scrollTop() < 60) {
			$("body").removeClass("scrolled");
			
		}
	}, 500);

	var activeUrl = window.location;
	$('a[href="'+activeUrl+'"]').parent('li').addClass('active');

	$(document).ready(function(){
		$("#phone").inputmask("99-9999999");
		$("#phone").inputmask({
			mask: "(99) 99999-9999",
		});
		
	});
		
})(jQuery);


$(document).ready(function() {

	$( "#form" ).submit(function( e ) {
		e.preventDefault();
	});
	
	$( "form" ).submit(function( e ) {
		e.preventDefault();
	});
	
	$("#edit-email").focusout(function(){

        const email = $("#edit-email").val();

		const validateEmail =   /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

		if( validateEmail.test(email) == true ){
			$(".sendEmail").addClass('active');
			$('#form').submit()
		} 
    });

	$('.hideCardNews').parent().hide()

	// Limitador de caracteres
	$('.timeline_content .description p').addClass('show-less-content')
	$('.timeline_content .description p').addClass('add-read-more')

	$('.convenience_card .information_convenience p').addClass('show-less-content')
	$('.convenience_card .information_convenience p').addClass('add-read-more-infra')

});


jQuery(function ($) {
	
	function AddReadMore() {
	   var carLmt = 105;
 
 
	   $(".add-read-more").each(function () {
		  if ($(this).find(".first-section").length)
			 return;
 
		  var allstr = $(this).text();
		  if (allstr.length > carLmt) {
			  var firstSet = allstr.substring(-1, carLmt);
			  var secdHalf = allstr.substring(carLmt, allstr.length);
			  var strtoadd = firstSet + "<span class='second-section'>" + secdHalf + "</span>" + "<span class='read-more'  title='Clique para ver mais'></span><span class='read-less' title='Clique para ver menos'></span>";
			  $(this).html(strtoadd);
		  }
	   });
 
	   $(".read-more,.read-less").click(function(){
		  $(this).closest(".add-read-more").toggleClass("show-less-content show-more-content");
		  
	   });
			
	}
 
	AddReadMore();
 });

jQuery(function ($) {
	
	function AddReadMore() {
	   var carLmt = 90;
 
 
	   $(".add-read-more-infra").each(function () {
		  if ($(this).find(".first-section").length)
			 return;
 
		  var allstr = $(this).text();
		  if (allstr.length > carLmt) {
			  var firstSet = allstr.substring(-1, carLmt);
			  var secdHalf = allstr.substring(carLmt, allstr.length);
			  var strtoadd = firstSet + "<span class='second-section'>" + secdHalf + "</span>" + "<span class='read-more'  title='Clique para ver mais'></span><span class='read-less' title='Clique para ver menos'></span>";
			  $(this).html(strtoadd);
		  }
	   });
 
	   $(".read-more,.read-less").click(function(){
		  $(this).closest(".add-read-more-infra").toggleClass("show-less-content show-more-content");
		  
	   });
			
	}
 
	AddReadMore();
 });

