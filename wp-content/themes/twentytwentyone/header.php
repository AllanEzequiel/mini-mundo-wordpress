<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> <?php twentytwentyone_the_html_classes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<link href="<?php echo get_template_directory_uri(); ?>/lightboxed/lightboxed.css" rel="stylesheet" />
	<script src="<?php echo get_template_directory_uri(); ?>/lightboxed/lightboxed.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.inputmask.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/inline.js"></script>
	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<header>
		<nav class="navegation">	
				<a class="logo_menu" href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri(); ?>/images/miniMundo-logo.png" alt="Mini mundo logo">
				</a>
				<div class="mobile-menu">
					<span></span>
					<span></span>
					<span></span>
				</div>
			<?php 
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'items_wrap'      => '<ul id="primary-menu-list" class="%2$s">%3$s</ul>',
						'fallback_cb'     => false,
					)
				);
			?>
		</nav>
		<div class="knowMore_contact">
			<a href="https://web.whatsapp.com/send?phone=555432864055" target="_blank"></a>
			<a class="icontickets" href="https://loja.minimundo.com.br/" target="_blank">Ingressos</a>
		</div>	
	</header>